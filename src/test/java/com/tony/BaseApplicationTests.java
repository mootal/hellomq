package com.tony;

import com.tony.entity.Log;
import com.tony.mq.rabbitmq.stream.log.LogSender;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseApplicationTests {

    private static Logger logger = LoggerFactory.getLogger(BaseApplicationTests.class);

//    @Autowired
//    @InjectMocks
//    private LogService logService;
//
//    @Mock
//    private LogRepository logRepository;

    @Autowired
    private LogSender sender;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    private static Log log(Boolean isSave) {
        if (isSave) {
            Log log = new Log();
            log.setTaskId(1L);
            log.setMessage("TEST MQ");
            log.setTime(System.currentTimeMillis());
            return log;
        } else {
            Log log = new Log();
            log.setTaskId(0L);
            log.setMessage("TEST MQ");
            log.setTime(System.currentTimeMillis());
            return log;
        }
    }

    @Test
    public void sendMQ() throws Exception {
//        sender.send();
        sender.sendMq(log(true));
    }

//    @Test
//    public void save() throws Exception {
//        Log log = logService.save(log(true));
//        Assert.assertThat(log.getMessage(), Matchers.is("TEST MQ"));
//    }
//
//    @Test
//    public void find() throws Exception {
//        logService.findByTaskId(100L);
//        Log log = log(false);
//        when(logRepository.findByTaskId(any())).thenReturn(log);
//        logger.info("findByTaskId:{}", log);
//        Assert.assertThat(log.getMessage(), Matchers.is("TEST MQ"));
//
//        logService.findByTime(1516765944L);
//        log = log(false);
//        when(logRepository.findByTime(any())).thenReturn(log);
//        logger.info("findByTime:{}", log);
//        Assert.assertThat(log.getMessage(), Matchers.not("TEST MQ1"));
//    }

}
