package com.tony.controller;

import com.tony.entity.Log;
import com.tony.mq.rabbitmq.stream.coffee.DrinkService;
import com.tony.mq.rabbitmq.stream.log.LogSender;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MessageQueueController {

    @Autowired
    private LogSender logSender;

    @Autowired
    private DrinkService drinkService;

    @ApiOperation(value = "推送日志到消息队列", notes = "推送日志消息", httpMethod = "POST")
    @PostMapping("/sendMQ")
    public void sendMq(Log log) {
        logSender.sendMq(log);
    }

    @ApiOperation(value = "制作饮品", notes = "饮品", httpMethod = "POST")
    @PostMapping("/makeDrink")
    public void makeDrink(/*Drink type*/) {
        drinkService.write();
    }
}
