package com.tony.mq.rabbitmq.boot;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQConfig {

    /**
     * 创建一个队列
     * @return
     */
    @Bean
    public Queue helloQueue() {

        return new Queue("hello");
    }

}
