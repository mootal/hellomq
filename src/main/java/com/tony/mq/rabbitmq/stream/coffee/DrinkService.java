package com.tony.mq.rabbitmq.stream.coffee;

import com.tony.util.FunctionAdaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@EnableBinding(Barista.class) // 收发如果分开 都要各自绑定
public class DrinkService implements FunctionAdaptor {

    private static DrinkService drinkService = new DrinkService();

    @Autowired
    private ApplicationContext bean;

    @Autowired
    private Barista server;

    public static void main(String[] args) {
        ((Barista) drinkService.applySupplier(drinkService::getServer)).hotDrinks().send(MessageBuilder.withPayload(System.currentTimeMillis() + " 狗子在吗?").build());
    }

    private Barista getServer() {
        return bean.getBean(Barista.class);
    }

    /**
     * 收
     *
     * @param message
     * @param header
     */
    @StreamListener(Barista.ORDER)
    public void handler(String message/*Message message*/, @Headers Map<String, Object> header) {
        System.out.println(System.currentTimeMillis() + " 时收到饮品：" + message);
//        System.out.print("狗子收到message消息：" + message.getPayload());
        System.out.println("消息header：" + header);
    }

    /**
     * 发
     *
     * @return
     */
    public boolean write() {
        server.hotDrinks().send(MessageBuilder.withPayload(System.currentTimeMillis() + " 时制作的热饮").build());
        System.out.println("制作热饮");
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {

        }
        server.coldDrinks().send(MessageBuilder.withPayload(System.currentTimeMillis() + " 时制作的冷饮").build());
        System.out.println("制作冷饮");
        return true;
    }


}
