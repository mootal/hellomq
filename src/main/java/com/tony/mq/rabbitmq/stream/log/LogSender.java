package com.tony.mq.rabbitmq.stream.log;

import com.tony.entity.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@EnableBinding(OutputLogChannel.class)
public class LogSender {

    private static Logger logger = LoggerFactory.getLogger(LogSender.class);

    @Autowired
    @Qualifier(value = OutputLogChannel.OUTPUT)
    public MessageChannel logChannel;

    public void sendMq(Log log) {
        try {
            logger.info("Send LLL to RabbitMQ : {}", log);
            logChannel.send(MessageBuilder.fromMessage(new GenericMessage<>(log)).build());
        } catch (Exception e) {
            logger.info("Failed send log to RabbitMQ : {}", e);
        }
    }

}
