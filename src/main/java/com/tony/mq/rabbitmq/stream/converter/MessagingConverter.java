package com.tony.mq.rabbitmq.stream.converter;

import com.tony.entity.Log;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.util.MimeType;

import java.lang.reflect.Method;

public class MessagingConverter extends MappingJackson2MessageConverter {

    private MessagingConverter() {
    }

    public MessagingConverter(MimeType... supportedMimeTypes) {
        super(supportedMimeTypes);
    }

    private static final String SUPPORT_METHOD = "convert";

    /**
     * 只支持含有指定SUPPORT_METHOD方法的类，用于反射回调
     *
     * @param aClass input class
     * @return boolean
     */
    @Override
    protected boolean supports(Class<?> aClass) {
        return getConvertMethod(aClass, String.class) != null || getConvertMethod(aClass, aClass) != null;
    }

    private Method getConvertMethod(Class<?> aClass, Class<?>... parameterTypes) {
        try {
            return aClass.getMethod(SUPPORT_METHOD, parameterTypes);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    @Override
    protected Object convertToInternal(Object payload, MessageHeaders headers, Object conversionHint) {
        logger.info("Convert To Internal");
        return payload.toString().getBytes(); // TODO
//        Class<?> view = this.getSerializationView(conversionHint);
//        return Optional.ofNullable(getConvertMethod(view, String.class)).map(m -> {
//            try {
//                return m.invoke(view.newInstance(), payload.toString());
//            } catch (Exception e) {
//                logger.error("Convert error : {}", e);
//                return null;
//            }
//        }).orElse(null);
    }

    @Override
    protected Object convertFromInternal(Message<?> message, Class<?> targetClass, Object conversionHint) {
        logger.info("Convert From Internal");
        Object payload = message.getPayload();
        return (payload instanceof Log ? payload : new Log(/*(byte[]) payload*/)); // TODO
    }

}
