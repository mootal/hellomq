package com.tony.mq.rabbitmq.stream.log;

import com.tony.entity.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.data.mongodb.core.mapping.Document;

@EnableBinding(InputLogChannel.class)
public class LogReceiver {

    private static Logger logger = LoggerFactory.getLogger(LogReceiver.class);

    @StreamListener(InputLogChannel.INPUT)
    public void receiveMq(Log message) {
        logger.info("Receive LLL from RabbitMQ : {}", message);
        try {
            logger.info("Saved log to DB : {}", Log.class.getAnnotation(Document.class).collection());
        } catch (Exception e) {
            logger.info("Failed save log to DB : {}", e);
        }
    }

}
