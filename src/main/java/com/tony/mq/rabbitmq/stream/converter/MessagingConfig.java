package com.tony.mq.rabbitmq.stream.converter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;

/**
 * 自定义converter
 */
@Configuration
public class MessagingConfig {

    @Bean
    public MessageConverter customMessageConverter() {
        return new MessagingConverter();
    }
}
