package com.tony.mq.rabbitmq.stream.coffee;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * 默认是方法名
 */
public interface Barista /*extends Processor*/ {

    String ORDER = "drink";
    String HOT = "hotDrinks";
    String COLD = "coldDrinks";

    @Input(ORDER)
    SubscribableChannel drink();

    @Output(HOT)
    MessageChannel hotDrinks();

    @Output(COLD)
    MessageChannel coldDrinks();

}
