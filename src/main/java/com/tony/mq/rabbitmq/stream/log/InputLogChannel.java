package com.tony.mq.rabbitmq.stream.log;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface InputLogChannel {

    String INPUT = "LOG";

    @Input(InputLogChannel.INPUT)
    SubscribableChannel in();
}
