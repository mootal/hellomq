package com.tony.mq.rabbitmq.stream.log;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface OutputLogChannel {

    String OUTPUT = "LOG";

    @Output(OutputLogChannel.OUTPUT)
    MessageChannel out();
}
