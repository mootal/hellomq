//package com.tony.mq.rabbitmq.simple;
//
//import com.rabbitmq.client.Channel;
//import com.rabbitmq.client.Connection;
//import com.rabbitmq.client.ConnectionFactory;
//import com.rabbitmq.client.QueueingConsumer;
//
///**
// * 2代向3代改造
// * @author dell
// *
// */
//public class Recv2_3 {
//	//队列名称
////    private final static String QUEUE_NAME = "workqueue2_3";
//    //转换器
//	private final static String EXCHANGE_NAME = "ex_log2_3_1";
//
//    public static void main(String[] argv) throws java.io.IOException,
//            java.lang.InterruptedException
//    {
//        //区分不同工作进程的输出
//        int hashCode = Recv2.class.hashCode();
//        //创建连接和频道
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("localhost");
//        Connection connection = factory.newConnection();
//        Channel channel = connection.createChannel();
//        //声明队列
//        boolean durable = true;// 1、设置队列持久化
//        // 2代 队列式
////        channel.queueDeclare(QUEUE_NAME, durable, false, false, null);
//        // 3代 交换式
//		channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
//		// 创建一个非持久的、唯一的且自动删除的队列
//		String queueName = channel.queueDeclare().getQueue();
//		// 为转发器指定队列，设置binding
//		channel.queueBind(queueName, EXCHANGE_NAME, "");
//
//        System.out.println("Test Case for rabbitMQ ***** " + hashCode
//                + " [*] Waiting for messages. To exit press CTRL+C");
//        // 3代把下面注掉 关键
////        channel.basicQos(1);//设置最大服务转发消息数量
//        QueueingConsumer consumer = new QueueingConsumer(channel);
//        // 指定消费队列
//        boolean ack = false; // 打开应答机制
//        //2 gen
////        channel.basicConsume(QUEUE_NAME, ack, consumer);
//        //3 gen
//        channel.basicConsume(queueName, true, consumer);
//
//        while (true)
//        {
//            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
//            String message = new String(delivery.getBody());
//
//            System.out.println(hashCode + " [x] Received '" + message + "'");
//            doWork(message);
//            System.out.println(hashCode + " [x] Done");
//
//            //发送应答
////            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
//        }
//
//    }
//
//    /**
//     * 每个点耗时1s
//     * @param task
//     * @throws InterruptedException
//     */
//    private static void doWork(String task) throws InterruptedException
//    {
////        for (char ch : task.toCharArray())
////        {
////            if (ch == '.')
////                Thread.sleep(1000);
////        }
//    }
//}
