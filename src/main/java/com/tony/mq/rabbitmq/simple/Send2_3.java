//package com.tony.mq.rabbitmq.simple;
//
//import java.io.IOException;
//
//import com.rabbitmq.client.Channel;
//import com.rabbitmq.client.Connection;
//import com.rabbitmq.client.ConnectionFactory;
//import com.rabbitmq.client.MessageProperties;
//
///**
// * 2代向3代改造
// * @author dell
// *
// */
//public class Send2_3 {
//	//队列名称
////    private final static String QUEUE_NAME = "workqueue2_3";
//    //转换器
//	private final static String EXCHANGE_NAME = "ex_log2_3_1";
//
//    public static void main(String[] args) throws IOException
//    {
//        //创建连接和频道
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("localhost");
//        Connection connection = factory.newConnection();
//        Channel channel = connection.createChannel();
//        //声明队列持久化
//        boolean durable = true;
//        // 2代 队列式
////        channel.queueDeclare(QUEUE_NAME, durable, false, false, null);
//        // 3代 交换式
//        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
//        //发送5条消息，依次在消息后面附加1-5个点
//        for (int i = 5; i > 0; i--)
//        {
//            String dots = "";
//            for (int j = 0; j <= i; j++)
//            {
//                dots += ".";
//            }
//            String message = "helloworld" + dots+dots.length();
//            // 2代 MessageProperties 2、设置消息持久化
////            channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
//            // 3代
//            channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
//            System.out.println(" [x] Sent '" + message + "'");
//        }
//        //关闭频道和资源
//        channel.close();
//        connection.close();
//
//    }
//}
