package com.tony.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.tony.config.Views;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "task_log")
public class Log implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @JsonView(Views.Manager.class)
    private String id;
    @JsonView(Views.Normal.class)
    private Long taskId;
    @JsonView(Views.Normal.class)
    private Long time;
    @JsonView(Views.Normal.class)
    private String message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Log{" +
                "\"taskId\":" + taskId +
                ", \"time\":" + time +
                ", \"message\":\"" + message + '"' +
                '}';
    }
}
