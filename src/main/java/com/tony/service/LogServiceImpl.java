package com.tony.service;

import com.tony.dao.LogRepository;
import com.tony.entity.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {

    private static Logger logger = LoggerFactory.getLogger(LogServiceImpl.class);

    @Autowired
    private LogRepository logRepository;

    @Override
    public List<Log> findAll() {
        logger.debug("Request to get all Logs");
        return logRepository.findAll();
    }

    @Override
    public Log findOne(String id) {
        logger.debug("Request to get Log : {}", id);
        return logRepository.findOne(id);
    }

    @Override
    public Log findByTaskId(Long taskId) {
        return logRepository.findByTaskId(taskId);
    }

    @Override
    public Log findByTime(Long time) {
        return logRepository.findByTime(time);
    }

    @Override
    public Log save(Log log) {
        log.setTime(System.currentTimeMillis());
        logRepository.save(log);
        return log;
    }

    @Override
    public Log update(Log log) {
        log.setTime(System.currentTimeMillis());
        logRepository.save(log);
        return log;
    }

    @Override
    public List<Log> query(Log log) {
        return logRepository.findAll();
    }

    @Override
    public void delete(Long taskId) {
        logRepository.delete(findByTaskId(taskId));
    }

}
