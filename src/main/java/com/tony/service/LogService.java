package com.tony.service;


import com.tony.entity.Log;

import java.util.List;

public interface LogService {

    /**
     * 保存日志信息
     *
     * @param log 日志信息
     */
    Log save(Log log);

    /**
     * 删除日志信息
     *
     * @param taskId 任务ID
     */
    void delete(Long taskId);

    /**
     * 修改日志信息
     *
     * @param log 日志信息
     */
    Log update(Log log);

    /**
     * 查询日志信息列表
     *
     * @param log 日志信息
     * @return 日志信息列表
     */
    List<Log> query(Log log);

    List<Log> findAll();

    Log findOne(String id);

    Log findByTaskId(Long taskId);

    Log findByTime(Long time);
}
