package com.tony.dao;

import com.tony.entity.Log;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends MongoRepository<Log, String> {

    Log findByTaskId(Long taskId);

    Log findByTime(Long time);
}
