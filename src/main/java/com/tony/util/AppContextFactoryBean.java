package com.tony.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Spring在代码中获取bean的几种方式 https://www.cnblogs.com/yjbjingcha/p/6752265.html
 * 方法一：在初始化时保存ApplicationContext对象
 * 方法二：通过Spring提供的utils类获取ApplicationContext对象
 * 方法三：继承自抽象类ApplicationObjectSupport
 * 方法四：继承自抽象类WebApplicationObjectSupport
 * 方法五：实现接口ApplicationContextAware
 * 方法六：通过Spring提供的ContextLoader
 */
@Component
public class AppContextFactoryBean implements ApplicationContextAware {

    /**
     * Spring应用上下文环境
     */
    private static ApplicationContext beanFactory;

    public static ApplicationContext getBeanFactory() {
        return beanFactory;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("--- Setting ApplicationContext ---");
        AppContextFactoryBean.beanFactory = applicationContext;
        System.out.println(this);
    }

    /**
     * In Any Class
     *
     * @param clazz
     * @return
     */
    public static <T> T getBean(Class<T> clazz) {
        return beanFactory.getBean(clazz);
    }

}
