package com.tony.util;

@FunctionalInterface
public interface Processor {

    void processor();

}
