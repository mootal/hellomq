package com.tony.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.*;

/**
 * 方法接口套接接口 T:parameter R:return
 * 使用者只需两步
 * 1 实现本接口
 * 2 增加静态类变量
 * Created by g29ti on 2017/9/7.
 */
public interface FunctionAdaptor<T, U, R> {

    /**
     * 统一日志拦截
     *
     * @param api
     * @param function
     * @return
     */
    default Object aop(Object api, Function function) {
        System.out.println("--- Start FunctionAdaptor AOP ---");
        long time = System.currentTimeMillis();
        Object r = null;
        try {
            r = function.apply(api);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Time Cost : " + (System.currentTimeMillis() - time));
        return r;
    }

    /**
     * 适配有入参，有返回值的方法，等效map
     *
     * @param param
     * @param api
     * @return
     */
    default R applyFunction(T param, Function<T, R> api) {
        return (R) aop(api, (a) -> api.apply(param));
    }

    /**
     * 适配有入参，没有返回值的方法
     *
     * @param param
     * @param api
     */
    default void applyConsumer(T param, Consumer<T> api) {
        aop(api, (a) -> {
            api.accept(param);
            return null;
        });
    }

    /**
     * 适配没有入参，有返回值的方法
     *
     * @param api
     * @return
     */
    default R applySupplier(Supplier<R> api) {
        return (R) aop(api, (a) -> api.get());
    }

    /**
     * 适配有入参，有返回值的方法，等效filter
     *
     * @param t
     * @param api
     * @return
     */
    default boolean applyPredicate(T t, Predicate<T> api) {
        return (Boolean) aop(api, (a) -> api.test(t));
    }

    /**
     * 适配没有入参，且没有返回值的方法，需自定义Processor函数式接口
     *
     * @param api
     */
    default void applyProcessor(Processor api) {
        aop(api, (a) -> {
            api.processor();
            return null;
        });
    }


// 复参数

    /**
     * 适配有双入参，有返回值的方法，等效map
     *
     * @param t
     * @param u
     * @param api
     */
    default R applyBiFunction(T t, U u, BiFunction<T, U, R> api) {
        return (R) aop(api, (a) -> api.apply(t, u));
    }

    /**
     * 适配有双入参，没有返回值的方法
     *
     * @param t
     * @param u
     * @param api
     */
    default void applyBiConsumer(T t, U u, BiConsumer<T, U> api) {
        aop(api, (a) -> {
            api.accept(t, u);
            return null;
        });
    }

    /**
     * 适配有双入参，有返回值的方法，等效filter
     *
     * @param t
     * @param u
     * @param api
     * @return
     */
    default boolean applyBiPredicate(T t, U u, BiPredicate<T, U> api) {
        return (Boolean) aop(api, (a) -> api.test(t, u));
    }

    /**
     * 演示类
     */
    class TT implements FunctionAdaptor {

        private static TT tt = new TT();

        private static Map<Integer, Integer> cache = new HashMap<>();

        /**
         * 静态方法现在可以调用非静态方法
         *
         * @param args
         */
        public static void main(String[] args) {
            cache.put(0, 0);
            cache.put(1, 1);

            int i1 = 55; // MAX 2147483647
            // 方法引用的写法：t1 = i1, so {t1 -> tt.function(t1)} = {tt::function}
            Object out1 = tt.applyFunction(i1, tt::function);
            Optional.ofNullable(out1).ifPresent(System.out::println);
            System.out.println();

            String i2 = /*null/""/"ERROR"*/"HELLO";
            // 入参必须是Object类型，否则编译不过，而返回类型则无限定，需要强转
            // {applyConsumer(i2, tt::consumer)} = {consumer(i2)} // TODO
            tt.applyConsumer(i2, tt::consumer);
            System.out.println();

            // 要注意方法的入参和返回类型
            double i3 = (Double) tt.applySupplier(tt::supplier);
            System.out.println(i3);
            System.out.println();

            boolean i4 = false;
            // 入参必须是Object类型，返回类型必须是boolean
            System.out.println(tt.applyPredicate(i4, tt::predicate));
            System.out.println();

            //
            tt.applyProcessor(tt::processor);
            System.out.println();

            // 复参数
            BigDecimal f1 = BigDecimal.valueOf(1), f2 = BigDecimal.valueOf(2);
            System.out.println(tt.applyBiFunction(f1, f2, tt::compare));
            System.out.println();

            //
            String c1 = "Hello", c2 = "World";
            tt.applyBiConsumer(c1, c2, tt::biConsumer);
            System.out.println();

            //
//            BigDecimal p1 = BigDecimal.valueOf(1), p2 = BigDecimal.valueOf(2);
//            System.out.println(tt.applyBiPredicate(i5, i6, tt::compare));
        }

        /**
         * 以下都是非静态方法
         */
        // 单参数
        public int function(Object in) {
            return cache.computeIfAbsent((int) in, (key) ->
                    function((int) in - 2) + function((int) in - 1));
        }

        public void consumer(Object in) {
            if (in == null) {
                throw new RuntimeException("null error");
            } else if (in.equals("")) {
                throw new RuntimeException("empty error");
            } else if (in.equals("ERROR")) {
                throw new RuntimeException("content error");
            } else if (in instanceof String) { // https://www.cnblogs.com/charlesblc/p/5992537.html
                System.out.println(in.hashCode());
            }
            System.out.println(in.toString().getBytes().hashCode());
        }

        public Double supplier() {
            return Math.random();
        }

        public Boolean predicate(Object in) {
            return in.equals(false);
        }

        public void processor() {
            ConcurrentHashMap map = new ConcurrentHashMap(1);
            map.put(1, 2);
            map.computeIfAbsent("NUM", k -> new LongAdder());
            System.out.println(map);
            map.computeIfPresent(3, tt::compare); // 可判断key是大于还是小于v 大1 等0 小-1
            System.out.println(map);
        }

        // 复参数
        public int compare(/*只能是Object，不能是BigDecimal*/Object a, Object b) {
            return ((BigDecimal.valueOf((Long.valueOf(a.toString()))))).compareTo(BigDecimal.valueOf((Long.valueOf(b.toString()))));
        }

        public void biConsumer(Object a, Object b) {
            ConcurrentHashMap map = new ConcurrentHashMap(1);
            map.putIfAbsent(a, b);
            System.out.println(map);
        }
    }
}
