package com.tony.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger配置类
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @SuppressWarnings("unchecked")
    @Bean
    public Docket docket() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2).groupName("kd-log-server")
                .genericModelSubstitutes(DeferredResult.class).useDefaultResponseMessages(false).forCodeGeneration(true)
                .pathMapping("/")// base，最终调用接口后会和paths拼接在一起
                .select().paths(Predicates.or(PathSelectors.regex("/.*")))// 过滤的接口
                .build().apiInfo(dataApiInfo());
        return docket;
    }

    private ApiInfo dataApiInfo() {
        return new ApiInfo("任务日志系统相关接口", // 大标题
                "任务日志系统相关接口.", // 小标题
                "1.0", // 版本
                "http://kd-log-server.kuandeng.com/kd-log-server/", "kd", // 作者
                "宽凳（北京）科技有限公司", // 链接显示文字
                "http://kd-log-server.kuandeng.com/"// 网站链接
        );
    }

}