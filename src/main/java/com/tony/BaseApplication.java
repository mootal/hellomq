package com.tony;

import com.tony.mq.rabbitmq.boot.RabbitSender;
import com.tony.mq.rabbitmq.stream.coffee.DrinkService;
import com.tony.util.AppContextFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * 文档
 * https://docs.spring.io/autorepo/docs/spring-cloud-stream-docs/Chelsea.SR2/reference/htmlsingle/#_consumer_properties
 * http://blog.csdn.net/phyllisy/article/details/51352868
 * http://blog.csdn.net/phyllisy/article/details/51382018
 * <p>
 * 入门
 * http://blog.csdn.net/lmj623565791/article/details/37706355
 * https://www.cloudamqp.com/blog/2015-09-03-part4-rabbitmq-for-beginners-exchanges-routing-keys-bindings.html
 * http://blog.csdn.net/fxq8866/article/details/62049393
 * http://www.rabbitmq.com/tutorials/amqp-concepts.html
 * <p>
 * 应用
 * http://blog.didispace.com/spring-boot-rabbitmq/
 * http://blog.didispace.com/spring-cloud-starter-dalston-7-1/
 * https://www.jianshu.com/p/730d86030a41
 * https://my.oschina.net/u/2342449/blog/1589297
 * https://www.jianshu.com/p/fb7d11c7f798
 * <p>
 * BUG
 * https://stackoverflow.com/questions/43216410/howto-set-routing-key-for-producer
 * https://github.com/spring-cloud/spring-cloud-stream-binder-rabbit/issues/57
 * https://github.com/spring-cloud/spring-cloud-stream-binder-aws-kinesis/issues/20
 */
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@SpringCloudApplication
@ComponentScan(basePackages = {"com.tony"}) // 为了把SpringUtils扫描进来 启动顺序比@Bean早
//@EnableBinding(value = { Barista.class }) // 这里也可以
public class BaseApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(BaseApplication.class, args);
        send(context);
        write(context);
    }

    // 发送消息
    public static void send(ConfigurableApplicationContext context) {
        RabbitSender service = context.getBean(RabbitSender.class);
        service.send();
    }

    // 发送消息
    public static boolean write(ConfigurableApplicationContext context) {
        DrinkService drinkService = context.getBean(DrinkService.class);
        System.out.println("START APP WRITE");
        return drinkService.write();
    }

}
